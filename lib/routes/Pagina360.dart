import 'package:flutter/material.dart';
import 'package:property_viewr/widgets/TurVirtualWebView.dart';
import 'package:property_viewr/widgets/MyMap.dart' as map;
import 'package:property_viewr/widgets/DescriereCasa.dart';
import 'package:property_viewr/widgets/BottomNavigationBar.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:property_viewr/widgets/IdCard.dart';
import 'package:flutter/cupertino.dart';

class Pagina360 extends StatefulWidget {
  final String titlu;
  final String urlTurVirtual;

  Pagina360({this.titlu, this.urlTurVirtual});

  @override
  _Pagina360State createState() => _Pagina360State();
}

class _Pagina360State extends State<Pagina360> {
  final firstColor = Color(0xff5b86e5), secondColor = Color(0xff36d1dc);
  Widget _body;
  int currentContent = 0;
  @override
  void initState() {
    _body = TurVirtual(url: widget.urlTurVirtual);
    super.initState();
  }

  void changeContentArea(int i) {
    switch (i) {
      case 0:
        {
          setState(() {
            _body = TurVirtual(url: widget.urlTurVirtual);
          });
        }
        break;
      case 1:
        {
          setState(() {
            _body = map.buildMap(context);
          });
        }
        break;
      case 2:
        {
          setState(() {
            _body = Descriere(context: context);
          });
        }
        break;
      case 3:
        {
          setState(() {
            _body = IdCard();
          });
        }
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(
          40,
        ),
        child: GradientAppBar(
          gradient: LinearGradient(
            colors: [firstColor, secondColor],
          ),
          actions: [
            IconButton(
                icon: Icon(
                  Icons.favorite,
                  color: Colors.white70,
                ),
                onPressed: () {
                  print('added to favorites');
                })
          ],
          title: AutoSizeText(
            '${widget.titlu}',
            maxFontSize: 15,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigation(
        f: changeContentArea,
      ),
      body: _body,
    );
  }
}
