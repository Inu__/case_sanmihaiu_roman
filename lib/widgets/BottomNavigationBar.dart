import 'package:flutter/material.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';

class BottomNavigation extends StatelessWidget {
  final Function f;
  BottomNavigation({this.f});

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(5.0),
      child: ConvexAppBar(
        items: [
          TabItem(icon: Icons.insert_emoticon, title: 'Vizualizare'),
          TabItem(icon: Icons.location_on, title: 'Locatie'),
          TabItem(icon: Icons.text_fields, title: 'Descriere'),
          TabItem(icon: Icons.add_call, title: 'Contact'),
        ],
        initialActiveIndex: 0, //optional, default as 0
        onTap: (int i) {
          print('click index=$i');
          f(i);
        },
      ),
    );
  }
}
