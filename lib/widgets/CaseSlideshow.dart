import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:property_viewr/widgets/StoryPage.dart';
import 'package:property_viewr/widgets/buttons.dart' as buttons;

class CaseSlideshow extends StatefulWidget {
  @override
  _CaseSlideshowState createState() => _CaseSlideshowState();
}

class _CaseSlideshowState extends State<CaseSlideshow> {
  final PageController ctrl = PageController(
    viewportFraction: 0.9,
  );
  final Firestore _db = Firestore.instance;
  Stream slides;
  String activeTag = 'toate';
  int currentPage;
  int currentCamere;
  int currentTeren = 80000;
  int currentConstruit = 900000;
  @override
  void initState() {
    _queryDb();
    print('Start page: $currentPage');
    ctrl.addListener(
      () {
        int next = ctrl.page.round();
        if (currentPage != next) {
          setState(() {
            currentPage = next;
            print('Changed page to: $currentPage');
          });
        }
      },
    );
    super.initState();
  }

  Stream _queryDb({int dormitoare = -1}) {
    Query query;
    if (dormitoare == -1) {
      query = _db.collection('case');
    } else if (dormitoare == 0) {
      query = _db.collection('case');
      query.getDocuments().then((value) {
        if (value.documents.length >= 1) {
          setState(() {});
          ctrl.nextPage(
            duration: Duration(
              milliseconds: 500,
            ),
            curve: Curves.easeInOutQuint,
          );
        } else {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(
              "Nu exista nicio proprietate. Reveniti",
            ),
          ));
        }
      });
    } else if (dormitoare == 2) {
      query = _db.collection('case').where(
            'dormitoare',
            isEqualTo: 2,
          );
      query.getDocuments().then((value) {
        if (value.documents.length >= 1) {
          setState(() {});
          ctrl.nextPage(
            duration: Duration(
              milliseconds: 500,
            ),
            curve: Curves.easeInOutQuint,
          );
        } else {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(
                "Nu exista proprietati cu $dormitoare dormitoare. Alegeti alta categorie."),
          ));
        }
      });
    } else if (dormitoare == 3) {
      query = _db.collection('case').where(
            'dormitoare',
            isEqualTo: 3,
          );
      query.getDocuments().then((value) {
        if (value.documents.length >= 1) {
          setState(() {});
          ctrl.nextPage(
            duration: Duration(
              milliseconds: 500,
            ),
            curve: Curves.easeInOutQuint,
          );
        } else {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(
                "Nu exista proprietati cu $dormitoare dormitoare. Alegeti alta categorie."),
          ));
          setState(() {
            currentCamere = -1;
          });
        }
      });
    } else if (dormitoare == 4) {
      query = _db.collection('case').where(
            'dormitoare',
            isGreaterThanOrEqualTo: 4,
          );
      query.getDocuments().then((value) {
        if (value.documents.length >= 1) {
          ctrl.nextPage(
            duration: Duration(
              milliseconds: 500,
            ),
            curve: Curves.easeInOutQuint,
          );
        } else {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(
                "Nu exista proprietati cu $dormitoare dormitoare. Alegeti alta categorie."),
          ));
        }
      });
    }
    // (tag == 'toate') ? query = _db.collection('case'):
    //       if (tag=='< 150.000 €'){query = _db.collection('case').where('pret',isLessThan: 150000,)}
    // query = query.where(
    //   'teren',
    //   isGreaterThanOrEqualTo: 750,
    // );
    slides =
        query.snapshots().map((snapshot) => snapshot.documents.map((document) {
              // if (document.data['pret'] <= 10) {
              return document.data;

              // } else {}
              //   return null;
            }));
    setState(() {
      currentCamere = dormitoare;
      // currentPage = 1;
    });
    return slides;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: slides,
      initialData: [],
      builder: (context, AsyncSnapshot snap) {
        // String searchType;
        // if (currentCamere == 0 && currentConstruit == 0 && currentTeren == 0) {
        // } else if (currentCamere == 0 && currentConstruit == 0) {
        // } else if (currentCamere == 0) {}

        List slideList = snap.data.toList();

        // List filteredList = slideList;
        // .retainWhere((item) => item['dormitoare'] <= currentCamere);
        // .retainWhere(
        //     (item) => item['suprafata_construita'] <= currentConstruit)
        // .retainWhere((item) => item['suprafata_teren'] <= currentTeren);
        // if (currentTeren != 0)
        //   slideList.forEach((element) {
        //     if (element['suprafata_teren'] > currentTeren) {
        //       filteredList.remove(element);
        //     }
        //     slideList = filteredList;
        //   });
        // if (currentCamere != 0)
        //   slideList.forEach((element) {
        //     if (element['dormitoare'] > currentCamere) {
        //       filteredList.remove(element);
        //     }

        //     slideList = filteredList;
        //   });
        // if (currentConstruit != 0)
        //   slideList.forEach((element) {
        //     if (element['suprafata_construita'] > currentConstruit) {
        //       filteredList.remove(element);
        //     }
        //     slideList = filteredList;
        //   });
        // slideList.forEach((element) {
        //   if (element['suprafata_construita'] > currentConstruit)
        //     slideList.remove(element);
        // });
        // slideList.forEach((element) {
        //   if (element['dormitoare'] > currentCamere) slideList.remove(element);
        // });
        // slideList.forEach((element) {
        //   print(element);
        // });
        return PageView.builder(
          // scrollDirection: Axis.vertical,
          controller: ctrl,
          itemCount: slideList.length + 1,
          itemBuilder: (context, int currentIndex) {
            Widget item;
            if (currentIndex == 0) {
              item = _buildTagPage();
            } else if (currentIndex == 1) {
              bool active = currentIndex == currentPage;
              print('index one!');
              item = StoryPage(
                data: slideList[currentIndex - 1],
                active: active,
              );
            } else if (slideList.length >= currentIndex) {
              // && slideList[currentIndex - 1] != null
              bool active = currentIndex == currentPage;
              item = StoryPage(
                data: slideList[currentIndex - 1],
                active: active,
              );
            }
            return item;
          },
        );
      },
    );
  }

  _buildTagPage() {
    // void priceChange(int camere) {
    //   if (currentCamere != camere)
    //     setState(() {
    //       currentCamere = camere;
    //       print(currentCamere);
    //     });
    // }

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AutoSizeText(
                  'Selecteaza',
                  maxFontSize: 40,
                  minFontSize: 20,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 20,
                ),
                buttons.RoomsButton(
                  camere: 0,
                  f: _queryDb,
                  currentCamere: currentCamere,
                ),
                buttons.RoomsButton(
                  camere: 2,
                  f: _queryDb,
                  currentCamere: currentCamere,
                ),
                buttons.RoomsButton(
                  camere: 3,
                  f: _queryDb,
                  currentCamere: currentCamere,
                ),
                buttons.RoomsButton(
                  camere: 4,
                  f: _queryDb,
                  currentCamere: currentCamere,
                ),
              ],
            ),
            // Column(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   crossAxisAlignment: CrossAxisAlignment.start,
            //   children: [
            //     AutoSizeText(
            //       'Camere',
            //       maxFontSize: 40,
            //       minFontSize: 20,
            //       style: TextStyle(fontWeight: FontWeight.bold),
            //     ),
            //     SizedBox(
            //       height: 20,
            //     ),
            //     _buildPriceButton('toate'),
            //     _buildPriceButton('< 150.000 €'),
            //     _buildPriceButton('> 150.000 €'),
            //   ],
            // ),
            // Column(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   crossAxisAlignment: CrossAxisAlignment.start,
            //   children: [
            //     AutoSizeText(
            //       'Dormitoare',
            //       maxFontSize: 40,
            //       minFontSize: 20,
            //       style: TextStyle(fontWeight: FontWeight.bold),
            //     ),
            //     SizedBox(
            //       height: 20,
            //     ),
            //     _buildNumarDeCamereButton(0),
            //     _buildNumarDeCamereButton(1),
            //     _buildNumarDeCamereButton(2),
            //     _buildNumarDeCamereButton(3),
            //     _buildNumarDeCamereButton(4),
            //   ],
            // ),
          ],
        ),
        // Row(
        //   crossAxisAlignment: CrossAxisAlignment.center,
        //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //   children: [
        //     Column(
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       crossAxisAlignment: CrossAxisAlignment.start,
        //       children: [
        //         AutoSizeText(
        //           'Teren',
        //           maxFontSize: 40,
        //           minFontSize: 20,
        //           style: TextStyle(fontWeight: FontWeight.bold),
        //         ),
        //         SizedBox(
        //           height: 20,
        //         ),
        //         _buildTerenButton(0),
        //         _buildTerenButton(600),
        //         _buildTerenButton(601),
        //       ],
        //     ),
        //     Column(
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       crossAxisAlignment: CrossAxisAlignment.start,
        //       children: [
        //         AutoSizeText(
        //           'Construit',
        //           maxFontSize: 40,
        //           minFontSize: 20,
        //           style: TextStyle(fontWeight: FontWeight.bold),
        //         ),
        //         SizedBox(
        //           height: 20,
        //         ),
        //         _buildConstruitButton(0),
        //         _buildConstruitButton(100),
        //         _buildConstruitButton(150),
        //         _buildConstruitButton(200),
        //         _buildConstruitButton(201),
        //       ],
        //     ),
        //   ],
        // ),
      ],
    );
  }

  // _StoryPage(Map data, bool active) {
  //   final double blur = active ? 30 : 0;
  //   final double offset = active ? 20 : 0;
  //   final double top = active ? 75 : 150;
  //   // ignore: unused_local_variable
  //   final double bottom = active ? 20 : 50;
  //   // print(data['pret']);
  //   return AnimatedContainer(
  //     duration: Duration(
  //       milliseconds: 500,
  //     ),
  //     curve: Curves.easeInOutQuint,
  //     margin: EdgeInsets.only(
  //       top: top,
  //       bottom: 100,
  //       right: 5,
  //       left: 10,
  //     ),
  //     decoration: BoxDecoration(
  //       borderRadius: BorderRadius.circular(20),
  //       boxShadow: [
  //         BoxShadow(
  //           color: Colors.black87,
  //           blurRadius: blur,
  //           offset: Offset(offset, offset),
  //         ),
  //       ],
  //       image: DecorationImage(
  //         fit: BoxFit.cover,
  //         image: NetworkImage(data['img']),
  //       ),
  //     ),
  //     child: Container(
  //       child: Padding(
  //         padding: const EdgeInsets.all(20.0),
  //         child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //           children: [
  //             Container(
  //               // color: Colors.black,
  //               decoration: BoxDecoration(
  //                 // color: Colors.black45,
  //                 borderRadius: BorderRadius.circular(20),
  //                 boxShadow: [
  //                   BoxShadow(
  //                     color: Colors.purple,
  //                     // blurRadius: 10,
  //                     // offset: Offset(0, 5),
  //                   ),
  //                 ],
  //               ),
  //               child: Padding(
  //                 padding: const EdgeInsets.all(8.0),
  //                 child: AutoSizeText(
  //                   ' ${data['title']} ',
  //                   minFontSize: 18,
  //                   maxLines: 3,
  //                   style: TextStyle(
  //                     color: Colors.white,
  //                   ),
  //                 ),
  //               ),
  //             ),
  //             SizedBox(
  //               height: 5,
  //             ),
  //             Container(
  //               decoration: BoxDecoration(
  //                 // color: Colors.black45,
  //                 borderRadius: BorderRadius.circular(20),
  //                 boxShadow: [
  //                   BoxShadow(
  //                     color: Colors.blue,
  //                     // blurRadius: 10,
  //                     // offset: Offset(0, 5),
  //                   ),
  //                 ],
  //               ),
  //               child: Padding(
  //                 padding: const EdgeInsets.all(8.0),
  //                 child: AutoSizeText(
  //                   ' €${data['pret']} ',
  //                   style: TextStyle(
  //                     color: Colors.white,
  //                     letterSpacing: 2,
  //                   ),
  //                 ),
  //               ),
  //             ),
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

}
