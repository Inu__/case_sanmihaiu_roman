import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';

class Descriere extends StatelessWidget {
  const Descriere({
    Key key,
    @required this.context,
  }) : super(key: key);

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  child: GFCard(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(
                            8.0,
                          ),
                          child: Icon(
                            Icons.weekend,
                            size: MediaQuery.of(context).size.width < 610
                                ? 25
                                : 50,
                            color: Colors.blue,
                          ),
                        ),
                        Text(
                          '4 Dormitoare',
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width < 610
                                ? 15
                                : 25,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    elevation: 2,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: GFCard(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.wc,
                            size: MediaQuery.of(context).size.width < 610
                                ? 25
                                : 50,
                            color: Colors.blue,
                          ),
                        ),
                        Text(
                          '2 Bai',
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width < 610
                                ? MediaQuery.of(context).size.width < 450
                                    ? 15
                                    : 18
                                : 25,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    elevation: 2,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  child: GFCard(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.nature,
                            size: MediaQuery.of(context).size.width < 610
                                ? 25
                                : 50,
                            color: Colors.blue,
                          ),
                        ),
                        Text(
                          '700m2 teren',
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width < 610
                                ? MediaQuery.of(context).size.width < 450
                                    ? 15
                                    : 18
                                : 25,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    elevation: 2,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: GFCard(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.aspect_ratio,
                            size: MediaQuery.of(context).size.width < 610
                                ? 25
                                : 50,
                            color: Colors.blue,
                          ),
                        ),
                        Text(
                          '155mp construiti',
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width < 610
                                ? MediaQuery.of(context).size.width < 450
                                    ? 15
                                    : 18
                                : 25,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    elevation: 2,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  child: GFCard(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.directions_car,
                            size: MediaQuery.of(context).size.width < 610
                                ? 25
                                : 50,
                            color: Colors.blue,
                          ),
                        ),
                        Text(
                          '1 Garaj',
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width < 610
                                ? MediaQuery.of(context).size.width < 450
                                    ? 15
                                    : 18
                                : 25,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    elevation: 2,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: GFCard(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.euro_symbol,
                            size: MediaQuery.of(context).size.width < 610
                                ? 25
                                : 50,
                            color: Colors.lightGreen,
                          ),
                        ),
                        Text(
                          '56 000',
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width < 610
                                ? MediaQuery.of(context).size.width < 450
                                    ? 15
                                    : 18
                                : 25,
                            color: Colors.lightGreen[900],
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    elevation: 2,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
