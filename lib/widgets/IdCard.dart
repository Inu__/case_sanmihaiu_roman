import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class IdCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 20,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(
                height: 15,
              ),
              CircleAvatar(
                radius: 75,
                backgroundImage: AssetImage('assets/eu.jpg'),
              ),
              SizedBox(
                height: 15,
              ),
              AutoSizeText(
                'Andreea Diana Crețiu',
                minFontSize: 25,
                maxFontSize: 200,
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'Montserrat',
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                AutoSizeText(
                  'Consultant Imobiliar de Reprezentare',
                  minFontSize: 16,
                  maxFontSize: 40,
                  style: TextStyle(
                    color: Colors.black87,
                    fontFamily: 'SourceSansPro',
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w100,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                // Text(
                //   'Fotografie 360°,  Online Advertising',
                //   style: TextStyle(
                //     color: Colors.black87,
                //     fontFamily: 'SourceSansPro',
                //     fontStyle: FontStyle.normal,
                //     fontWeight: FontWeight.w100,
                //   ),
                // ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 40,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      4,
                    ),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 0.01,
                        spreadRadius: 0,
                        offset: Offset(0, 0), // shadow direction: bottom right
                      )
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 16.0,
                        ),
                        child: Container(
                          child: IconButton(
                            icon: Icon(
                              Icons.phone,
                              size: 40,
                              color: Colors.blue,
                            ),
                            onPressed: () {},
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      AutoSizeText(
                        '+40 771 272 810',
                        minFontSize: 18,
                        maxFontSize: 100,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// Card(
//                 color: Colors.white,
//                 margin: EdgeInsets.symmetric(horizontal: 20),
//                 child: Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: ListTile(
//                     contentPadding: EdgeInsets.symmetric(
//                       horizontal: 10,
//                       vertical: 0,
//                     ),
//                     leading: Icon(
//                       Icons.phone,
//                       color: Colors.teal[900],
//                       size: 30,
//                     ),
//                     title: AutoSizeText(
//                       '+40 773 325 312',
//                       minFontSize: 20,
//                       style: TextStyle(
//                         fontWeight: FontWeight.w700,
//                       ),
//                     ),
//                   ),
//                 ),
//               )
