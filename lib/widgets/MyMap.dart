import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

Widget buildMap(BuildContext context) {
  // LatLng location = LatLng(35.68, 51.41);
  FlutterMap map = FlutterMap(
    options: new MapOptions(
      // center: location,
      zoom: 16.0,
      center: LatLng(
        45.707760,
        21.090610,
      ),
    ),
    layers: [
      new TileLayerOptions(
        urlTemplate:
            "https://api.mapbox.com/styles/v1/calincretiu/ckazd42cb0ja21ilhdj7j7zdo/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiY2FsaW5jcmV0aXUiLCJhIjoiY2themNseGVoMGNtMDJycGowaGM4YjQyayJ9.eTarI60FEC-OFIaMpZD2CA",
        additionalOptions: {
          'accessToken':
              'pk.eyJ1IjoiY2FsaW5jcmV0aXUiLCJhIjoiY2themNseGVoMGNtMDJycGowaGM4YjQyayJ9.eTarI60FEC-OFIaMpZD2CA',
          'id': 'mapbox.terrain-rgb',
        },
      ),
      new MarkerLayerOptions(
        markers: [
          new Marker(
            width: 80.0,
            height: 80.0,
            point: LatLng(
              45.707760,
              21.090610,
            ),
            builder: (ctx) => new Container(
              child: Icon(
                Icons.add_location,
                color: Colors.white,
                size: 60,
              ),
            ),
          ),
        ],
      ),
    ],
  );
  return map;
}
