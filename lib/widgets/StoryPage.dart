import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:nice_button/nice_button.dart';
import 'package:property_viewr/routes/Pagina360.dart';

class StoryPage extends StatefulWidget {
  final Map data;

  final bool active;
  StoryPage({this.data, this.active});
  @override
  _StoryPageState createState() => _StoryPageState();
}

class _StoryPageState extends State<StoryPage> {
  var firstColor = Color(0xff5b86e5), secondColor = Color(0xff36d1dc);

  _storyPage(Map data, bool active) {
    final double blur = active ? 30 : 0;
    final double offset = active ? 20 : 0;
    final double top = active ? 2 : 100;
    // ignore: unused_local_variable
    final double bottom = active ? 20 : 50;
    // print(data['pret']);
    return AnimatedContainer(
      duration: Duration(
        milliseconds: 50,
      ),
      curve: Curves.easeInOutQuint,
      margin: EdgeInsets.only(
        top: top,
        bottom: 2,
        right: 5,
        left: 5,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.black87,
            blurRadius: blur,
            offset: Offset(offset, offset),
          ),
        ],
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(data['img']),
        ),
      ),
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                // color: Colors.black,
                decoration: BoxDecoration(
                  // color: Colors.black45,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.purple,
                      // blurRadius: 10,
                      // offset: Offset(0, 5),
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: AutoSizeText(
                    ' ${data['title']} ',
                    minFontSize: 18,
                    maxLines: 3,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: AutoSizeText(
                        ' €${data['pret']} ',
                        style: TextStyle(
                          color: Colors.white,
                          letterSpacing: 2,
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      constraints: BoxConstraints(minWidth: 100, maxWidth: 600),
                      child: NiceButton(
                          background: Colors.white,
                          radius: 40,
                          padding: const EdgeInsets.all(15),
                          text: "Detalii",
                          // icon: Icons.account_box,
                          gradientColors: [secondColor, firstColor],
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Pagina360(
                                  titlu: data['title'],
                                  urlTurVirtual: data['urlTurVirtual'],
                                ),
                              ),
                            );
                          }),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _storyPage(widget.data, widget.active),
    );
  }
}
