import 'package:flutter/material.dart';
import 'package:web_browser/web_browser.dart';

class TurVirtual extends StatelessWidget {
  final String url;
  TurVirtual({this.url});
  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      heightFactor: 1,
      widthFactor: 1,
      child: Center(
        child: WebBrowser(
          interactionSettings: WebBrowserInteractionSettings(
            topBar: null,
            bottomBar: null,
            // bottomBar: FloatingActionButton(
            //   onPressed: () {
            //     print('pressed');
            //   },
            // ),
          ),
          javascriptEnabled: true,
          iframeSettings: WebBrowserIFrameSettings(
            height: '100%',
            width: '100%',
          ),
          key: ValueKey('browser'),
          initialUrl: '$url',
        ),
      ),
    );
  }
}
