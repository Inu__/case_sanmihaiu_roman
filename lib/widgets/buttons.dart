import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';

class RoomsButton extends StatefulWidget {
  final int camere;
  final Function f;
  final int currentCamere;
  RoomsButton({this.camere, this.f, this.currentCamere});
  @override
  _RoomsButtonState createState() => _RoomsButtonState();
}

class _RoomsButtonState extends State<RoomsButton> {
  FlatButton _buildNumarDeCamereButton({int camere, int currentCamere}) {
    Color color = camere == currentCamere ? Colors.purple : Colors.blue;
    String buttonText;
    if (camere == 0)
      buttonText = 'Oricate';
    else
      buttonText = camere.toString();
    return FlatButton(
      onPressed: () {
        widget.f(dormitoare: widget.camere);
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: AutoSizeText(
          '$buttonText dormitoare',
          maxFontSize: 20,
          minFontSize: 15,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      color: color,
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildNumarDeCamereButton(
      camere: widget.camere,
      currentCamere: widget.currentCamere,
    );
  }
}

// _buildPriceButton(String tag, Function f, String activeTag) {
//   Color color = tag == activeTag ? Colors.purple : Colors.blue;
//   return FlatButton(
//     onPressed: f,
//     child: Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Text(
//         '$tag',
//         style: TextStyle(
//           color: Colors.white,
//         ),
//       ),
//     ),
//     color: color,
//   );
// }
// _buildTerenButton(int teren) {
//   Color color = teren == currentTeren ? Colors.purple : Colors.blue;
//   String buttonText;
//   if (teren == 0)
//     buttonText = 'Toate';
//   else if (teren <= 600) buttonText = '<600 m2';
//   if (teren > 600) buttonText = '>600 m2';
//   return FlatButton(
//     onPressed: () {
//       setState(() {
//         currentTeren = teren;
//       });
//     },
//     child: Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Text(
//         '$buttonText',
//         style: TextStyle(
//           color: Colors.white,
//         ),
//       ),
//     ),
//     color: color,
//   );
// }
// _buildConstruitButton(int construit) {
//   Color color = construit == currentConstruit ? Colors.purple : Colors.blue;
//   String buttonText;
//   if (construit == 0)
//     buttonText = 'Toate';
//   else if (construit <= 100) buttonText = '0 - 100 m2';
//   if (construit <= 150 && construit > 100) buttonText = '100 - 150 m2';
//   if (construit <= 200 && construit > 150) buttonText = '150 - 200 m2';
//   if (construit > 200) buttonText = 'peste 200 m2';

//   return FlatButton(
//     onPressed: () {
//       setState(() {
//         currentConstruit = construit;
//       });
//     },
//     child: Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Text(
//         '$buttonText',
//         style: TextStyle(
//           color: Colors.white,
//         ),
//       ),
//     ),
//     color: color,
//   );
// }
